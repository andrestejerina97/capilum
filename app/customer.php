<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class customer extends Model
{
    public function users()
    {
        return $this->hasMany('App\User');
    }

    public function getConfig($path)
    {
       $query= $this->hasOne(Config::class,"customer_id","id")->where("path",$path)->first();
       return $query?$query->value:"";
    }
}
