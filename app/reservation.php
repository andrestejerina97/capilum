<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class reservation extends Model
{
    use SoftDeletes;

    protected $table="reservations";
    protected $guarded=["id","created_at","modified_at"];

    public function services()
    {
        return $this->belongsTo('App\service','service_id','id')->withDefault([
            'name' => 'Eliminado',
        ]);
    }
    public function employees()
    {
        return $this->belongsTo('App\employee','employee_id','id')->withDefault([
            'name' => 'Eliminado',
        ]);
    }
    public function pacients()
    {
        return $this->belongsTo('App\pacient','pacient_id','id')->withDefault([
            'name' => 'Eliminado',
        ]);
    }
    public function statuses()
    {
        return $this->belongsTo('App\status','status_id','id');
    }
    
    public function scopeCustomer($query,$customer_id)
    {
        return $query->where('customer_id',$customer_id);
    }

    public function scopeTitle($query,$param)
    {
        return $query->where('Title','LIKE','%'.$param.'%');
    }
    public function scopeDate($query,$dateAt,$dateEnd)
    {
        if ($dateAt != null and $dateEnd != null) {
            return $query->whereBetween("date_at",[$dateAt,$dateEnd]);
        }
    }
    public function scopeService($query,$param)
    {
        if ($param != null) {
            return $query->where('service_id',$param);
        }
    }
    public function scopeEmployee($query,$param)
    {
        if ($param != null) {
            return $query->where('employee_id',$param);
        }
    }
    public function scopeTime($query,$timeAt,$timeEnd)
    {
        if ($timeAt != null and $timeEnd != null) {
            return $query->whereBetween("time_at",[$timeAt,$timeEnd]);
        }
    }
    public function pacient()
    {
        return $this->belongsTo('App\pacient','pacient_id','id')->withDefault([
            'name' => 'Cliente',
            "email"=>"Eliminado"
        ]);
    }
    
    
}
