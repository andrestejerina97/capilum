<?php

namespace App\Task;

use App\customer;
use App\Mail\ConfirmReservation;
use App\Mail\ReminderReservation;
use App\reservation;
use App\status;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class SendEmailConfirmReservationTask 
{
    /**
     * Execute the console command.
     *
     * @return int
     */
    public function __invoke($forDay=false)
    {
        $customers=customer::join("configs","configs.customer_id","customers.id")
        ->select("customers.*")
        ->where("path","email/sendConfirmReservation")
        ->where("value",1)
        ->get();

        if(count($customers)>0){

            $now= date("Y-m-d");
            foreach ($customers as $customer) {
                $statusPending=status::where("customer_id",$customer->id)
                ->where("name","Pendiente")
                ->first();
                $reservations=reservation::where("email_confirm",0)
                ->where("customer_id",$customer->id)
                ->where("created_at",">",$now." 00:00:00") //Obtenemos solo las de hoy para no traer registros muy viejos
                ->where("status_id",$statusPending->id)->get();
                foreach ($reservations as $reservation) {
                    try {

                    $textBody=$customer->getConfig("email/confirmReservationMessage");
        
                    $textBody= str_replace("@nombre_cliente",$reservation->pacient->name,$textBody);
                    $date = Carbon::parse($reservation->date_at);

                    $textBody= str_replace("@fecha_turno ",$this->getDateSpanish($date,$reservation->time_at),$textBody);
                    $name=$customer->name." ".$customer->city;
                    $mailable = new ConfirmReservation($textBody,$name);
                        if($reservation->pacient && $reservation->pacient->email && $reservation->pacient->email != "Eliminado" ){
                            $emailTo=filter_var($reservation->pacient->email, FILTER_SANITIZE_EMAIL);
                            $reservation->email_confirm=1;
                            if ($emailTo) {
                                Mail::to($emailTo)
                                ->send($mailable);
                            }
    
                        }
                    } catch (\Throwable $th) {
                        Log::error("Error al enviar email: ". $reservation->id);
                        Log::error("Error al enviar email: ". $th->getMessage());
                        throw $th;
                    }
                    $reservation->save();
                    echo "Enviado";

                }

            }
        }

    }

    public function getDateSpanish($date,$time)
    {
        $months = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
        $month = $months[($date->format('n')) - 1];
        return $date->format('d') . ' de ' . $month . ' de ' . $date->format('Y') . ' a las '. $time;
    }
}
