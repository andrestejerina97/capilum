<?php

namespace App\Task;

use App\customer;
use App\Mail\ReminderReservation;
use App\reservation;
use App\status;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;

class SendEmailRecordatoryReservationTask
{
    /**
     * Execute the console command.
     *
     * @return int
     */
    public function __invoke($forDay = false)
    {

        $customers = customer::join("configs", "configs.customer_id", "customers.id")
            ->select("customers.*")
            ->where("path", "email/sendReminderMessage")
            ->where("value", 1)
            ->get();
        if (count($customers) > 0) {

            $now = Carbon::now();
            foreach ($customers as $customer) {
                $statusPending = status::where("customer_id", $customer->id)
                    ->where("name", "Pendiente")
                    ->first();
                $reservations = reservation::where("date_at",$now->copy()->modify("+1 day")->format('Y-m-d'))
                    ->where("customer_id", $customer->id)
                    ->where("email_reminder", 0)
                    ->where("status_id", $statusPending->id)->get();
                foreach ($reservations as $reservation) {
                    try {

                        $date = Carbon::parse($reservation->date_at);
                        $time = Carbon::parse($reservation->date_at." ".$reservation->time_at);

                        //verifico que la hora de envio esté entre la hora del turno y 20 min mas
                        if($time->between($now->copy()->modify("+1 day"), $now->copy()->modify("+1 day")->modify("+20 minute"))){
                            $textBody = $customer->getConfig("email/reminderMessage");
                            $textBody = str_replace("@nombre_cliente", $reservation->pacient->name, $textBody);
                            $textBody = str_replace("@fecha_turno ", $this->getDateSpanish($date, $reservation->time_at), $textBody);
                            $mailable = new ReminderReservation($textBody, $customer->name);
    
                            if ($reservation->pacient && $reservation->pacient->email && $reservation->pacient->email != "Eliminado") {
                                Mail::to($reservation->pacient->email)
                                    ->send($mailable);
                            }
                            $reservation->email_reminder = 1;
                            $reservation->save();
                        }
                      
                    } catch (\Throwable $th) {
                        var_dump($reservation->date_at);
                        throw $th;
                    }


                    echo "Enviado";
                }
            }
        }
    }

    public function getDateSpanish($date, $time)
    {
        $months = array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
        $month = $months[($date->format('n')) - 1];
        return $date->format('d') . ' de ' . $month . ' de ' . $date->format('Y') . ' a las ' . $time;
    }
}
