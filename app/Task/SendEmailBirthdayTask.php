<?php

namespace App\Task;

use App\customer;
use App\Mail\ReminderReservation;
use App\reservation;
use App\status;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;

class SendEmailBirthdayTask 
{
    /**
     * Execute the console command.
     *
     * @return int
     */
    public function __invoke($forDay=false)
    {

       

    }

    public function getDateSpanish($date,$time)
    {
        $months = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
        $month = $months[($date->format('n')) - 1];
        return $date->format('d') . ' de ' . $month . ' de ' . $date->format('Y') . ' a las '. $time;
    }
}
