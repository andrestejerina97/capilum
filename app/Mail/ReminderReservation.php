<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ReminderReservation extends Mailable
{
    use Queueable, SerializesModels;

    /***
     * String $TextBody
     */
    public $textBody;
    
    /***
     * String $TextBody
     */
    public $customerName;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($textBody,$customerName)
    {
        $this->textBody = $textBody;
        $this->customerName = $customerName;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
        ->from('norepply@gmail.com',$this->customerName)
        ->subject('Recordatorio de turno')
        ->markdown('admin.email.reminders.reservation');
    }
}
