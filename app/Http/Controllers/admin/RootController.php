<?php

namespace App\Http\Controllers\admin;

use App\Config;
use App\customer;
use App\employee;
use App\Http\Controllers\Controller;
use App\pacient;
use App\pay_method;
use App\reservation;
use App\service;
use App\Services\ConfigService;
use App\status;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;

class RootController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $customer_id= Auth::user()->customer_id;
        $employees=employee::Customer($customer_id)->orderBy('id','desc')->get();
        $services=service::Customer($customer_id)->orderBy('id','desc')->get();
        $pacients=pacient::Customer($customer_id)->orderBy('id','desc')->get();
        $reservations=reservation::Customer($customer_id)->NoneStatus('Anulado')->with(['services',"statuses","pacients","employees"])->orderBy('id','desc')->get();
        return view('admin.index',compact('reservations','pacients','services','employees'));
    }
    public function root()
    {

      if(auth::user()->hasRole("root")){
        $customers=customer::with('users')->orderBy('id','asc')->get();
        User::with('customers')->orderBy('id','asc')->get();
       return view('admin.customer.index')
       ->with('customers',$customers);
      
      }else{
        abort(404);
      }
 
    }
    public function edit($id)
    {
      $customers=customer::with('users')->where('id',$id)->orderBy('id','asc')->get();
      return view('admin.customer.edit')
       ->with('customers',$customers);
    }
    public function update(Request $request)
    {
          $data=$request->except('_token','photos');
          $customer= customer::where('id',$data['customer'])->firstOrFail();
          $customer->keyname=$data['keyname'];
          $customer->description=$data['description'];
          $customer->city=$data['city'];
          $customer->name=$data['name'];
          $customer->amount=$data['amount'];
      
          //$customer->backgroundColor=$data['backgroundColor'];
          //$customer->textColor=$data['textColor'];
          $customer->save();
          ConfigService::syncConfig($customer->id);

          if($request->has('photos')){
            $files=$request->file('photos');
               if ($files) {
                $path="storage/customer/".$customer->id."/";
                $customer->logo=$path."logo".".".$files->getClientOriginalExtension();
                $path = $files->storeAs('customer/'.$customer->id,"logo".".".$files->getClientOriginalExtension());
                $customer->save(); 
               }           
            }
          return redirect()->route('root');
    }

    public function save(Request $request)
    {
      if(auth::user()->hasRole('root')){
        if($request->has('id')){
          $data= $request->except('_token','photo','password_confirmation','password');
  
          if ($request->has('password') && $request->input('password') == '') {
            $validatedData = $request->validate([
              'name' => ['required', 'string', 'max:255'],
              'username' => ['required',Rule::unique('users')->ignore($request->input('id'))],
            ],
            [
              'username.unique' => "El nombre de usuario ya está en uso,por favor introduzca uno nuevo",
            ]
          );
  
   
  
        }else{
          
            $validatedData = $request->validate([
              'name' => ['required', 'string', 'max:255'],
              'username' => ['required',Rule::unique('users')->ignore($request->input('id'))],
              'password' => ['required', 'string', 'confirmed'],
            ],
            [
              'username.unique' => "El nombre de usuario ya está en uso,por favor introduzca uno nuevo",
            ]
          );
          $data['password']=Hash::make($request->input('password'));  
  
          }
          
          if ($request->has('is_active')) {
            $data['is_active']=1;
          }else{
            $data['is_active']=0;
          }
          if ($request->has('is_admin')) {
            $data['is_admin']=1;
            $user=User::find($request->input('id'));
            $user->syncRoles("admin");
          }else{
            $data['is_admin']=0;
            $user=User::find($request->input('id'));
            $user->syncRoles("user");
          }
            $user=User::where('id','=',$request->input('id'))->update($data);
  
      
            return response()->json(['result'=>$request->input('id')]);
  
     
        }else{
          $validatedData = $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'password' => ['required', 'string', 'confirmed'],
          ],
          [
            'username.unique' => "El nombre de usuario ya está en uso,por favor introduzca uno nuevo",
          ]
        );
          $data= $request->except('_token','photo','role');
          $data['password']=Hash::make($request->input('password'));

          $customer=new customer();
          $customer->name=$data['name'];
          $customer->is_active=1;
          $customer->save();
          Config::insert([
            [
                "name"=>"Mensaje recordatorio de cumpleaños",
                "path"=>"email/birthdayMessage",
                "value"=>"Estimado @nombre_cliente te deseamos un feliz cumpleaños de parte de CALENDY SA",
                "customer_id"=>$customer->id
            ],
            [
                "name"=>"Mensaje recordatorio de turnos",
                "path"=>"email/reminderMessage",
                "value"=>"Estimado @nombre_cliente te recordamos que tenés un turno reservado para el día @fecha_turno ,  te esperamos en nuestro local",
                "customer_id"=>$customer->id
            ],
            [
                "name"=>"Habilitar envío automático de mensaje de cumpleaños",
                "path"=>"email/sendBirthdayMessage",
                "value"=>"0",
                "customer_id"=>$customer->id
            ],
            [
                "name"=>"Habilitar envío automático de recordatorio de turnos",
                "path"=>"email/sendReminderMessage",
                "value"=>"0",
                "customer_id"=>$customer->id
            ]
          ]);
          status::create([
              'name'=>'Pagado',
              'customer_id'=>$customer->id,
          ]);
          status::create([
              'name'=>'Pendiente',
              'customer_id'=>$customer->id,
          ]);
          status::create([
              'name'=>'Anulado',
              'customer_id'=>$customer->id,
          ]);
          status::create([
            'name'=>'Realizado',
            'customer_id'=>$customer->id,
        ]);

          pay_method::insert([
            ["name_method"=>"Efectivo", "customer_id"=>$customer->id],
            ["name_method"=>"Tarjeta", "customer_id"=>$customer->id]
          ]);
          $data['customer_id']=$customer->id;
  
          if ($request->has('is_active')) {
            $data['is_active']=1;
          }else{
            $data['is_active']=0;
          }
          if ($request->has('is_admin')) {
            $data['is_admin']=1;
            $user=User::create($data);
            $user->assignRole("admin");
          }else{
            $data['is_admin']=0;
            $user=User::create($data);
            $user->assignRole("user");
    
          }
        }
        ConfigService::syncConfig();

       }
          return redirect()->route('root');
    }

    public function crear(Request $request)
    {
         $customer=Auth::user()->customers->id;

         $estados= customer::all();

         foreach ($estados as $c) {
           
          status::insert([
            'name'=>'Realizado',
            'customer_id'=>$c->id
          ]);

         }
    }
}
