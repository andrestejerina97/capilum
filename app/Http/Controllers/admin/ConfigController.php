<?php

namespace App\Http\Controllers\admin;

use App\Config;
use App\Http\Controllers\Controller;
use App\reservation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

class ConfigController extends Controller
{
  public function index()
  {

    $configs = Config::Customer(Auth::user()->customer_id)->orderBy('id', 'desc')->get();
    return view('admin.configs.index', compact('configs'));
  }
  public function edit($config_id = 0)
  {
    if ($config_id == 0) {
      return view('admin.configs.edit');
    } else {

      $configs = config::where('configs.id', '=', $config_id)->get();
      return view('admin.configs.edit')
        ->with('configs', $configs);
    }
  }

  public function store(Request $request)
  {
    $data = $request->except('_token', 'id','startTime','endTime','startTime_1','endTime_1');
    if ($request->has('id')) {
      $config = config::where("id", '=', $request->input('id'))->update($data);
      return response()->json(['result' => $request->input('id')]);
    } else {
      $data['customer_id'] = Auth::user()->customer_id;
      $config = config::create($data);
      return response()->json(['result' => $config->id]);
    }
  }
  public function search(Request $request)
  {
    $datas = $request->except('_token');
    $configs = config::Customer(Auth::user()->customer_id)
      ->Lastname($datas['lastname'])
      ->Name($datas['name'])
      ->orderBy('id', 'DESC')->get();
    return view('admin.configs.index', compact('configs'));
  }

  public function delete($id)
  {
    if ($id != 'id') {
      $user = config::find($id);
      $user->delete();
      return response()->json(['result' => 1]);
    } else {
      return response()->json(['result' => -1]);
    }
  }

  public function show($config_id)
  {
    $customer_id = Auth::user()->customer_id;

    $reservations = reservation::Customer(Auth::user()->customer_id)
      ->Service(null)
      ->config($config_id)
      ->with(['services',"statuses","configs","employees"])
      ->orderBy('id', 'DESC')->paginate(20);

    return view('admin.configs.show', compact('reservations'));
  }
}
