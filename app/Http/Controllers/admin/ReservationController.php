<?php

namespace App\Http\Controllers\admin;

use App\employee;
use App\Http\Controllers\Controller;
use App\pacient;
use App\reservation;
use App\service;
use App\status;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;

class ReservationController extends Controller
{
  public function index(Request $request)
  {
    $customer_id = Auth::user()->customer_id;
    $employees = employee::Customer($customer_id)->orderBy('lastname', 'asc')->get();
    $services = service::Customer($customer_id)->orderBy('id', 'desc')->get();
    $pacients = pacient::Customer($customer_id)->orderBy('id', 'desc')->get();
    $datas = $request->except('_token');
    $filterService = isset($datas['service_id']) ? $datas['service_id'] : null;
    $filterEmployee = isset($datas['employee_id']) ? $datas['employee_id'] : null;
    $filterOrderDate = isset($datas['orderDate']) ? $datas['orderDate'] : null;
    $filterOrderTime = isset($datas['orderTime']) ? $datas['orderTime'] : null;
    $filterTimeAt = isset($datas['time_at']) ? $datas['time_at'] : null;
    $filterTimeEnd = isset($datas['time_at_end']) ? $datas['time_at_end'] : null;
    $dateAt = isset($datas['date_at']) ? $datas['date_at'] : null;
    $dateEnd = isset($datas['date_at_end']) ? $datas['date_at_end'] : null;
    $reservations = reservation::Customer($customer_id)
      ->Service($filterService)
      ->Employee($filterEmployee)
      ->Date($dateAt, $dateEnd)
      ->Time($filterTimeAt, $filterTimeEnd)
      ->with('services')
      ->with('pacients')
      ->with('employees');
    if ($request->input("orderDate", false) and in_array($request->orderDate, ["asc", "desc"])) {
      $reservations = $reservations->orderBy(DB::raw('DATE(date_at)', 'date_at'), $request->orderDate);
    }
    if ($request->input("orderTime", false) and in_array($request->orderTime, ["asc", "desc"])) {
      $reservations = $reservations->orderBy("time_at", $request->orderTime);
    }
    $reservations = $reservations->paginate(10)->withQueryString();

    return view('admin.reservations.index', compact('reservations', 'pacients', 'services', 'employees', 'datas'));
  }
  public function edit($reservation_id = 0, $date = 0)
  {
    $customer_id = Auth::user()->customer_id;

    if ($reservation_id == 0) {

      $employees = employee::Customer($customer_id)->orderBy('id', 'desc')->get();
      $services = service::Customer($customer_id)->orderBy('id', 'desc')->get();
      $pacients = pacient::Customer($customer_id)->orderBy('id', 'desc')->get();
      $statuses = status::Customer($customer_id)->orderBy('id', 'desc')->get();

      if ($date == 0) {
        $date = date("Y-m-d");
      }
      // $reservations=reservation::orderBy('id','desc')->get();
      return view('admin.reservations.edit', compact('statuses', 'pacients', 'services', 'employees'))
        ->with('date', $date);
    } else {

      $employees = employee::Customer($customer_id)->orderBy('id', 'desc')->get();
      $services = service::Customer($customer_id)->orderBy('id', 'desc')->get();
      $pacients = pacient::Customer($customer_id)->orderBy('id', 'desc')->get();
      $statuses = status::Customer($customer_id)->orderBy('id', 'desc')->get();
      $reservations = reservation::with('services')->where('reservations.id', '=', $reservation_id)->get();
      return view('admin.reservations.edit', compact('reservations', 'statuses', 'pacients', 'services', 'employees'));
    }
  }

  public function store(Request $request)
  {
    $data = $request->except('_token', 'id', 'phone','email');
    if ($request->has('id')) {
      //validation reservation IMPORTANT!!
      //   $start = Carbon::createFromTimeString($request->input('time_at'), 'America/Argentina/Buenos_Aires');
      //   $end = Carbon::createFromTimeString($request->input('time_end'), 'America/Argentina/Buenos_Aires');
      $reservation_validates = reservation::where('date_at', $request->input('date_at'))
        ->where('employee_id', $data['employee_id'])
        ->select('time_end', 'time_at', 'id')
        ->get();
      $count = 0;
      foreach ($reservation_validates as $reservation_validate) {
        if ($reservation_validate->id != $request->input('id')) {
          $start = Carbon::createFromTimeString($reservation_validate['time_at'], 'America/Argentina/Buenos_Aires');
          $end = Carbon::createFromTimeString($reservation_validate['time_end'], 'America/Argentina/Buenos_Aires');
          if ($start->between(Carbon::createFromTimeString($request->input('time_at'), 'America/Argentina/Buenos_Aires'), Carbon::createFromTimeString($request->input('time_end'), 'America/Argentina/Buenos_Aires'))) {
            $count = 1;
          } else {
            if ($end->between(Carbon::createFromTimeString($request->input('time_at'), 'America/Argentina/Buenos_Aires'), Carbon::createFromTimeString($request->input('time_end'), 'America/Argentina/Buenos_Aires'))) {
              $count = 1;
            }
          }
        }
      }

      if ($count > 0) {
        return response()->json(['result' => -1, 'message' => "La fecha y hora de la cita se encuentra ocupada actualmente, por favor seleccione otro día u horario"]);
      }
      //end validation to reservation
      $data['customer_id'] = Auth::user()->customer_id;

      if (!is_numeric($request->pacient_id)) {
        $pacient = pacient::create([
          'name' => $request->pacient_id,
          'customer_id' => $data['customer_id'],
          'phone' => $request->input('phone', ''),
          'email' => $request->input('email', '')
        ]);
        $data['pacient_id'] = $pacient->id;
      } else {
        pacient::where('id', $request->pacient_id)->update(['phone' => $request->phone,'email'=>$request->email]);
      }
      $reservation = reservation::where("id", '=', $request->input('id'))->update($data);

      return response()->json(['result' => $request->input('id')]);
    } else {
      //validation reservation IMPORTANT!!
      $start = Carbon::createFromTimeString($request->input('time_at'), 'America/Argentina/Buenos_Aires');
      $end = Carbon::createFromTimeString($request->input('time_end'), 'America/Argentina/Buenos_Aires');
      $reservation_validates = reservation::where('date_at', $request->input('date_at'))
        ->where('employee_id', $data['employee_id'])
        ->select('time_end', 'time_at')
        ->get();
      $count = 0;
      foreach ($reservation_validates as $reservation_validate) {
        $start = Carbon::createFromTimeString($reservation_validate['time_at'], 'America/Argentina/Buenos_Aires');
        $end = Carbon::createFromTimeString($reservation_validate['time_end'], 'America/Argentina/Buenos_Aires');
        if ($start->between(Carbon::createFromTimeString($request->input('time_at'), 'America/Argentina/Buenos_Aires'), Carbon::createFromTimeString($request->input('time_end'), 'America/Argentina/Buenos_Aires'))) {
          $count = 1;
        } else {
          if ($end->between(Carbon::createFromTimeString($request->input('time_at'), 'America/Argentina/Buenos_Aires'), Carbon::createFromTimeString($request->input('time_end'), 'America/Argentina/Buenos_Aires'))) {
            $count = 1;
          }
        }
      }

      if ($count > 0) {
        return response()->json(['result' => -1, 'message' => "La fecha y hora de la cita se encuentra ocupada actualmente, por favor seleccione otro día u horario"]);
      }
      //end validation to reservation
      // validation new customer/patient
      $data['customer_id'] = Auth::user()->customer_id;

      if (!is_numeric($request->pacient_id)) {
        $pacient = pacient::create([
          'name' => $request->pacient_id,
          'customer_id' => $data['customer_id'],
          'email' => $request->input('email', ''),
          'phone' => $request->input('phone', '')
        ]);
        $data['pacient_id'] = $pacient->id;
      } else {
        pacient::where('id', $request->pacient_id)->update(['phone' => $request->phone,'email' => $request->email]);
      }
      //End validation new customer/patient
      $reservation = reservation::create($data);
      return response()->json(['result' => $reservation->id]);
    }
  }

  public function delete($id)
  {
    if ($id != 'id') {
      $user = reservation::find($id);
      $user->delete();
      return response()->json(['result' => 1]);
    } else {
      return response()->json(['result' => -1]);
    }
  }

  public function search(Request $request)
  {
    $customer_id = Auth::user()->customer_id;
    $datas = $request->except('_token');

    $reservations = reservation::Customer(Auth::user()->customer_id)
      ->Service($datas['service_id'])
      ->Employee($datas['employee_id'])
      ->Date($datas['date_at'])
      ->orderBy('id', 'DESC')->get();

    $employees = employee::Customer($customer_id)->orderBy('id', 'desc')->get();
    $services = service::Customer($customer_id)->orderBy('id', 'desc')->get();
    $pacients = pacient::Customer($customer_id)->orderBy('id', 'desc')->get();
    $statuses = status::Customer($customer_id)->orderBy('id', 'desc')->get();
    return view('admin.reservations.index', compact('reservations', 'pacients', 'services', 'employees'));
  }

  public function gridDay(Request $request, $day)
  {
    $customer_id = Auth::user()->customer_id;

    $employees = employee::Customer($customer_id)->orderBy('id', 'asc')->get();
    $reservationsForEmployee = [];
    $times = array();
    $now = Carbon::createFromFormat("Y-m-d",$day)->startOfDay();
    $now->modify("+7 hours");
    $period = 30; // 15 minutos de intervalo
    $numberRows = ((60 / $period) * 16) + 5;
    $rows = array();
    $reservations = reservation::Customer($customer_id)
      ->with('services')
      ->with('employees')
      ->where('date_at', $now->format("Y-m-d"))
      ->orderBy("employee_id", "ASC")
      ->orderBy("time_at","ASC")
      ->get();
    for ($i = 1; $i <= $numberRows; $i++) {
      $rows[$i]["time"] = $now->format("H:i");
      foreach ($employees as $employee) {
        $rows[$i]["reservations"][$employee->id]= array();
        foreach ($reservations as $reservation) {
          $auxTime = Carbon::createFromFormat("Y-m-d H:i",$day." ".$reservation->time_at);
          $nowMoreMinutes = $now->copy()->modify("+" . $period . " minutes");
          if ($auxTime->gte($now) && $auxTime->lt($nowMoreMinutes) && $reservation->employee_id == $employee->id) {
            $rows[$i]["reservations"][$employee->id][] = $reservation;
          }
        }
      }
      $now->modify("+" . $period . " minutes");

    }
    return view('admin.reservations.gridDay', compact('rows', 'employees'));
  }
}
