<?php

namespace App\Http\Controllers\admin;

use App\customer;
use App\employee;
use App\Http\Controllers\Controller;
use App\pacient;
use App\reservation;
use App\service;
use App\status;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $customer_id= Auth::user()->customer_id;
        $employees=employee::Customer($customer_id)->orderBy('id','desc')->get();
        $services=service::Customer($customer_id)->orderBy('id','desc')->get();
        $pacients=pacient::Customer($customer_id)->orderBy('id','desc')->get();
        $reservations=reservation::Customer($customer_id)->with('services')->with('pacients')->with('employees')->orderBy('id','desc')->get();
        return view('admin.index',compact('reservations','pacients','services','employees'));
    }
    public function rootIndex()
    {
      if(auth::user()->customers->name=='administrador'){
        $customers=customer::with('users')->orderBy('id','asc')->get();

        
        User::with('customers')->orderBy('id','asc')->get();
       return view('admin.customer.index')
       ->with('customers',$customers);
      
      }else{
        abort(404);
      }
 
    }
    public function editIndex($id)
    {
      $customers=customer::with('users')->where('id',$id)->orderBy('id','asc')->get();
      return view('admin.customer.edit')
       ->with('customers',$customers);
    }
    public function updateIndex(Request $request)
    {
          $data=$request->except('_token','photos');
          $customer= customer::where('id',$data['customer'])->firstOrFail();
          $customer->keyname=$data['keyname'];
          $customer->description=$data['description'];
          $customer->city=$data['city'];
          $customer->name=$data['name'];
          $customer->amount=$data['amount'];
          $customer->save();
          if($request->has('photos')){
            $files=$request->file('photos');
               if ($files) {
                $path="storage/customer/".$customer->id."/";
                $customer->logo=$path."logo".".".$files->getClientOriginalExtension();
                $path = $files->storeAs('customer/'.$customer->id,"logo".".".$files->getClientOriginalExtension());
                $customer->save(); 
               }           
            }
          return redirect()->route('root');
    }

    public function saveIndex(Request $request)
    {
      if(auth::user()->customers->name=='administrador'){
        if($request->has('id')){
          $data= $request->except('_token','photo','password_confirmation','password');
  
          if ($request->has('password') && $request->input('password') == '') {
            $validatedData = $request->validate([
              'name' => ['required', 'string', 'max:255'],
              'username' => ['required',Rule::unique('users')->ignore($request->input('id'))],
            ],
            [
              'username.unique' => "El nombre de usuario ya está en uso,por favor introduzca uno nuevo",
            ]
          );
  
   
  
        }else{
          
            $validatedData = $request->validate([
              'name' => ['required', 'string', 'max:255'],
              'username' => ['required',Rule::unique('users')->ignore($request->input('id'))],
              'password' => ['required', 'string', 'confirmed'],
            ],
            [
              'username.unique' => "El nombre de usuario ya está en uso,por favor introduzca uno nuevo",
            ]
          );
          $data['password']=Hash::make($request->input('password'));  
  
          }
          
          if ($request->has('is_active')) {
            $data['is_active']=1;
          }else{
            $data['is_active']=0;
          }
          if ($request->has('is_admin')) {
            $data['is_admin']=1;
            $user=User::find($request->input('id'));
            $user->syncRoles("admin");
          }else{
            $data['is_admin']=0;
            $user=User::find($request->input('id'));
            $user->syncRoles("user");
  
          }
            $user=User::where('id','=',$request->input('id'))->update($data);
  
      
            return response()->json(['result'=>$request->input('id')]);
  
     
        }else{
          $validatedData = $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'password' => ['required', 'string', 'confirmed'],
          ],
          [
            'username.unique' => "El nombre de usuario ya está en uso,por favor introduzca uno nuevo",
          ]
        );
          $data= $request->except('_token','photo','role');
          $data['password']=Hash::make($request->input('password'));

          $customer=new customer();
          $customer->name=$data['name'];
          $customer->is_active=1;
          $customer->save();

          status::create([
              'name'=>'Pagado',
              'customer_id'=>$customer->id,
          ]);
          status::create([
              'name'=>'Pendiente',
              'customer_id'=>$customer->id,
          ]);
          status::create([
              'name'=>'Anulado',
              'customer_id'=>$customer->id,
          ]);
          status::create([
            'name'=>'Realizado',
            'customer_id'=>$customer->id,
        ]);
          $data['customer_id']=$customer->id;
  
          if ($request->has('is_active')) {
            $data['is_active']=1;
          }else{
            $data['is_active']=0;
          }
          if ($request->has('is_admin')) {
            $data['is_admin']=1;
            $user=User::create($data);
            $user->assignRole("admin");
          }else{
            $data['is_admin']=0;
            $user=User::create($data);
            $user->assignRole("user");
    
          }
        }
  
       }
          return redirect()->route('root');
    }

    public function crear(Request $request)
    {
         $customer=Auth::user()->customers->id;

         $estados= customer::all();

         foreach ($estados as $c) {
           
          status::insert([
            'name'=>'Realizado',
            'customer_id'=>$c->id
          ]);

         }
    }
}
