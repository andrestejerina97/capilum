

@extends('layouts.admin')
@section('css')
<link href="{{asset('css/select2.min.css')}}" rel="stylesheet"/>

@endsection
@section('content')
@php 
$filterService=isset($datas['service_id'])?$datas['service_id']:null;
$filterEmployee=isset($datas['employee_id'])?$datas['employee_id']:null;
$filterOrderDate=isset($datas['orderDate'])?$datas['orderDate']:null;
$filterOrderTime=isset($datas['orderTime'])?$datas['orderTime']:null;
$filterDateAt=isset($datas['date_at'])?$datas['date_at']:null;
$filterDateEnd=isset($datas['date_at_end'])?$datas['date_at_end']:null;
$filterTimeAt=isset($datas['time_at'])?$datas['time_at']:null;
$filterTimeEnd=isset($datas['time_at_end'])?$datas['time_at_end']:null;
@endphp
<div class="row">
	<div class="col-md-12">
<div class="btn-group pull-right">
</div>

<div class="card">
  <div class="card-header" id="top">
      <h4 class="title" id="encabezados">Turnos</h4>
  </div>
  <div class="card-content ">
<a href="{{route('admin.edit.reservation')}}" class="btn btn-primary">Nuevo Turno</a>

<br><br>
  <form id="form-filter" class="form-horizontal" method="get" action="{{route('admin.index.reservation')}}" role="form">
  <div class="form-group">
    <div class="col-lg-2">
      <label for="inputEmail1" class="col-lg-2 control-label">Servicio</label>
    </div>
    <div class="col-lg-8">
      <select id="service_id" name="service_id" class="form-control select2" >
        <option value="" selected>-- SELECCIONE --</option>
        @foreach ($services as $service)
        <option data-duration="{{$service->duration}}" data-amount="{{$service->amount}}" value="{{$service->id}}"
        @if ($filterService == $service->id)
            selected
        @endif
          >{{$service->name}} </option>         
        @endforeach
          
      </select>
    </div>
  </div>
  <div class="form-group">
    <div class="col-lg-2">
      <label for="inputEmail1" class="col-lg-2 control-label">Encargado</label>
    </div>
    <div class="col-lg-8">
      <select id="employee_id" name="employee_id" class="form-control select2" >
        <option value="" selected>-- SELECCIONE --</option>
        @foreach ($employees as $employee)
        <option value="{{$employee->id}}"
          @if ($filterEmployee == $employee->id)
            selected
          @endif
          >{{$employee->name}} {{$employee->lastname}}</option>         
        @endforeach
      </select>
    </div>
  </div>
  <div class="form-group text-center">
    <div class="col-lg-2">
      <label for="inputEmail1" class="col-lg-2 control-label">Fechas:</label>
    </div>
    <div class="col-lg-3">
      <input type="date" class="form-control" name="date_at" value="{{$filterDateAt}}" id="date_at">
    </div>
    <div class="col-lg-3">
      <input type="date" class="form-control" name="date_at_end" value="{{$filterDateEnd}}"  id="date_end">
    </div>
  </div>
  <div class="form-group text-center">
    <div class="col-lg-2">
      <label for="inputEmail1" class="col-lg-2 control-label">Hora de inicio:</label>
    </div>
    <div class="col-lg-3">
      <input type="time" class="form-control" name="time_at" value="{{$filterTimeAt}}" id="time_at">
    </div>
    <div class="col-lg-3">
      <input type="time" class="form-control" name="time_at_end" value="{{$filterTimeEnd}}"  id="time_end">
    </div>
  </div>
  <div class="form-group text-center">
    <div class="col-lg-4">
      <a class="btn  btn-warning " href="{{route('admin.index.reservation')}}" style="margin: 0px 0px 20px 0px">LIMPIAR</a>
    </div>
    <div class="col-lg-4">
    	<button class="btn  btn-block" style="margin: 0px 0px 20px 0px">Buscar</button>
    </div>
  </div>
</form>
      @if (count($reservations)>0)
			<div class="table-responsive">
        <table class="table table-bordered table-hover">
          <thead>
          <th>Cliente</th>
          <th>Profesional</th>
          <th>Servicio</th>
          <th>Fecha  <a href="javascript:;" 
            class="order @if($filterOrderDate=="desc") hide @endif"  
            data-value="desc"
            data-field="orderDate"
            @if( $filterOrderDate=="desc") data-selected="true" @endif>
            <i class="fa fa-chevron-down"></i> </a>  
    
            <a href="javascript:;" 
            class="order @if($filterOrderDate=="asc") hide @endif"  
            data-value="asc" 
            data-field="orderDate" 
            @if( $filterOrderDate=="asc") data-selected="true" @endif>
            <i class="fa fa-chevron-up"></i></a>
          </th>
          
            <th>Hora Inicio <a href="javascript:;" 
              class="order @if( $filterOrderTime=="desc") hide @endif" 
              @if( $filterOrderTime=="desc") data-selected="true" @endif 
              data-value="desc" 
              data-field="orderTime">
              <i class="fa fa-chevron-down"></i> </a>  
            <a href="javascript:;" 
            class="order @if($filterOrderTime=="asc") hide @endif" 
            @if( $filterOrderTime=="asc") data-selected="true" @endif
            data-value="asc" 
            data-field="orderTime">
            <i class="fa fa-chevron-up"></i> </a>
          </th>
          <th>Hora Finalización</th>
    
          <th></th>
          </thead>
                @foreach($reservations as $reservation)
                <tr>
                <td>{{$reservation->pacients->name." ".$reservation->pacients->lastname}}</td>
                <td>{{$reservation->employees->name." ".$reservation->employees->lastname}}</td>
                <td>{{$reservation->services->name}}</td>
                <td>{{$reservation->date_at}}</td>
                <td>{{$reservation->time_at}}</td>
                <td>{{$reservation->time_end}}</td>
    
                <td style="width:280px;">
                <!--<a href="" class="btn btn-default btn-xs">Historial</a>-->
                <a href="{{route('admin.edit.reservation',$reservation->id)}}" class="btn btn-warning btn-xs">Editar</a>
                <a href="javascript:;" onclick="eliminar(this);" data-id_reservation={{$reservation->id}} class="btn btn-danger btn-xs">Eliminar</a>
                </td>
                </tr>
                @endforeach
                @else
                <p class='alert alert-danger'>No hay turnos</p>
                @endif
          </table>
      </div>
      {{$reservations->links()}}
			</div>
			</div>

	</div>
</div>
@endsection
@section('scripts')
<script src="{{asset('js/sweetalert.min.js')}}" type="text/javascript"></script>
<script src="{{asset('js/select2.min.js')}}" type="text/javascript"></script>
<script src="{{asset('js/es.js')}}" type="text/javascript"></script>

<script>
  
  $(document).on('click',".order",function(e){
    filterAndOrder(e,this)
  });

  function filterAndOrder(e,element) {
    var newUrl= "{{route('admin.index.reservation')}}";
    var form= new FormData($("#form-filter")[0])
    var queryString= new URLSearchParams(form).toString();
    newUrl=newUrl+"?"+$(element).data("field")+"="+$(element).data("value");

    $(".order").each(function(){
      if($(this).data("selected")==true && $(element).data("field") != $(this).data("field")){
      newUrl=newUrl+"&"+$(this).data("field")+"="+$(this).data("value");
      }
    })  
    $(element).data("selected","true");    
    location.href=newUrl+"&"+queryString;
  }

    $('.select2').select2({
  language: "es",
});
 
    function eliminar(a) {
          swal({
            icon: "warning",
              text: '¿Seguro que deseas eliminar este turno?,se borrarán todos los datos relacionados con él,esto no se  puede revertir!".',
              buttons: {
                  cancel: "No,cancelar!",
                  catch: {
                    text: "Sí,eliminar",
                    value: "catch",
                  },
                },
              })
              .then((value) => {
                switch (value) {
                  case "catch":
               let id_reservation=$(a).data("id_reservation");
               let url="{{route('admin.delete.reservation','id')}}"
               url=url.replace("id",id_reservation);
                $.ajax({
                url         : url,
                cache       : false,
                contentType : false,
                processData : false,
                type        : 'get',
                dataType:"JSON",
                success     : function(data, textStatus, jqXHR){
                    if (data.result != -1) {
                      swal({
                        tittle:"Excelente!",
                        text:"Turno eliminado con éxito",
                        icon :"success",
                      }).then((value) => {
                        location.href='{{route("admin.index.reservation")}}';


                      });

                    }else{

                    }
               
                  }
            });
                    break;
                  default:
                  
                }
              });
        

      }

    </script>    
@endsection


