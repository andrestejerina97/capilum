<div class="">
    @if (count($employees) > 0)
        <div class="table-responsive" style="height:100vh; overflow-y:scroll; overflow-x:scroll;">
            <table class="table table-bordered table-hover">
                <thead>
                    <tr>
                        <td></td>
                        @foreach ($employees as $employee)
                            @php
                                $bgcolor = $employee->color ? $employee->color : '#ffffff';
                            @endphp
                            <td style="background-color:{{ $bgcolor }}">{{ $employee->name }}</td>
                        @endforeach
                    </tr>

                </thead>
                <tbody >
                    @foreach ($rows as $row)
                        <tr>
                            <td>
                                {{ $row['time'] }}
                            </td>
                            @isset($row['reservations'])
                                @foreach ($row['reservations'] as $reservationsByemployee)
                                    <td>
                                        @foreach ($reservationsByemployee as $reservation)
                                            @php
                                                $bgcolor = $reservation->employees->color ?? '#fff';
                                            @endphp
                                            <a href="{{route("admin.edit.reservation",[$reservation->id,0])}}">
                                            <p style="color:#fff;padding:5px;border-radius: 8px; background-color:{{ $bgcolor }}">
                                                {{$reservation->services->name }},{{$reservation->pacients->name }} {{$reservation->pacients->lastname}}
                                                {{ $reservation->time_at }}</p></a>
                                        @endforeach
                                    </td>
                                @endforeach
                            @endisset
                        </tr>
                    @endforeach
                </tbody>

                @else
                    <p class='alert alert-danger'>No hay turnos</p>
    @endif
    </table>
</div>
