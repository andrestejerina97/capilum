@extends('layouts.admin')
@section('content')
<div class="row">
	<div class="col-md-12">
<div class="btn-group pull-right">

</div>
<div class="card">
  <div class="card-header" id="top">
      <h4 class="title" id="encabezados">Configuracioness</h4>
  </div>
  <div class="card-content table-responsive">
	
	<form method="get" action="{{route('admin.search.config')}}" role="form"  class="form-horizontal">
    @csrf
    <input type="hidden" name="view" value="configs">
		<div class="form-group">
			<div class="col-lg-3">
				<input type="text" name="name" placeholder="Nombre" class="form-control">
			</div>

			<div class="col-lg-1">
				
			</div>
			<div class="col-lg-2">
				<button class="btn " >Buscar</button>
			</div>
			<div class="col-lg-2">
                @role("root")
                <a href="{{route('admin.edit.config')}}" class="btn btn-primary"><i class='fa fa-male'></i> Nueva Configuración</a>
                @endrole
            </div>
			
		</div>
	</form>
			
			<table class="table table-bordered table-hover">
			<thead>
			<th>Nombre</th>
			<th></th>
            </thead>
            @if (count($configs)>0)
                
			@foreach($configs as $config)
				
				<tr>
                <td>{{$config->name}}</td>
				<td style="width:280px;">
                <!--<a href="" class="btn btn-default btn-xs">Historial</a>-->
                <a href="{{route('admin.edit.config',$config->id)}}" class="btn btn-warning btn-xs">Editar</a>
				</td>
				</tr>
            @endforeach
            @else
            <p class='alert alert-danger'>No hay registros</p>
            @endif

			</table>
			</div>
			</div>
	</div>
</div>
@endsection


@section('scripts')
<script src="{{asset('js/sweetalert.min.js')}}" type="text/javascript"></script>

<script>

function eliminar(a) {
          swal({
            icon: "warning",
              text: '¿Seguro que deseas eliminar este Configuraciones?,se borrarán todos los datos relacionados con él,esto no se  puede revertir!".',
              buttons: {
                  cancel: "No,cancelar!",
                  catch: {
                    text: "Sí,eliminar",
                    value: "catch",
                  },
                },
              })
              .then((value) => {
                switch (value) {
                  case "catch":
               let id_config=$(a).data("id_config");
               let url="{{route('admin.delete.config','id')}}"
               url=url.replace("id",id_config);
                $.ajax({
                url         : url,
                cache       : false,
                contentType : false,
                processData : false,
                type        : 'get',
                dataType:"JSON",
                success     : function(data, textStatus, jqXHR){
                    if (data.result != -1) {
                      swal({
                        tittle:"Excelente!",
                        text:"Configuraciones eliminado con éxito",
                        icon :"success",
                      }).then((value) => {
                        location.href='{{route("admin.index.config")}}';


                      });

                    }else{

                    }
               
                  },
				  error:function(data,message,res){
                    let lista= "<ul>";
                for(var k in data.responseJSON.errors) {
                lista += "<li>"+ data.responseJSON.errors[k][0] +"</li>";              
                }
                
              lista+="</ul>";
              let au='<div class="alert alert-danger msj" role="alert">'+lista+'</div>';

                      swal({
                      title: 'Ups!',
                      text: 'Hubo un error',
                      html: lista,
                      type: 'error',
                    });

                   // swal("ups!","Hubo un error al procesar tu petición,vuelve a intentar por favor","error");
                  }
            });
                    break;
                  default:
                  
                }
              });
        

      }   

</script>
@endsection