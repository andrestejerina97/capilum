@extends('layouts.admin')
@section('css')
 
<link href='{{asset('calendar5/main.css')}}' rel='stylesheet' />
<style>
.fc-button{
  background-color: blueviolet !important;
  margin-top: 5px !important;
  border: none !important;
}

@media (max-width: 400px) {
           
  .fc-button{
  background-color: blueviolet !important;
  margin-top: 5px !important;
  border: none !important;
  width: 80% !important;
  }
  .fc-toolbar-title{
    font-weight: 90% !important;

  }
          
}
</style>
@endsection
@section('content')
<div class="row">
	<div class="">
		<div class="card">
  		<div class="card-header" id="top">
        <h4 class="title" id="encabezados">MI AGENDA</h4>
  		</div>
  		<div class="table-responsive" >
      <div id="calendar"></div>

      </div>
      <br>
      <div style="display: flex;justify-content: center; flex-wrap: wrap">
        <div style="border-radius:50px;width: 30px; height: 20px;margin-right:2px; background-color: #ccc903">
        </div>
        <div style="margin-right: 5px">App</div>
       @foreach ($employees as $employee)
       <div style="border-radius:50px;width: 30px; height: 20px;margin-right:2px; background-color: {{$employee->color}}">
      </div>
      <div style="margin-right: 5px">{{$employee->name}}</div>
       @endforeach
      </div>
		</div>
	</div>
</div>
 
@endsection
@section('scripts')
<script src='{{asset("calendar5/main.js")}}'></script>
<script src='{{asset("calendar5/es.js")}}'></script>

<script>
    var calendarEl = document.getElementById('calendar');

        var calendar = new FullCalendar.Calendar(calendarEl, {
          themeSystem: 'standard',
          timeZone: 'local',
           initialDate: "{{date('Y-m-d')}}", // will be parsed as local
		   initialView: 'dayGridMonth',     
		   editable: false,
       
           selectable: true,
           businessHours: true,
		   locale: 'es',
           dayMaxEvents: false, // allow "more" link when too many events
		   eventColor: '#378006',
       headerToolbar: { 
             left: 'prev,next today',
             center: 'title',
             right: 'dayGridMonth,dayGridWeek,timeGridDay' ,// buttons for switching between views
            },
              eventDisplay: 'block',
              hour: 'numeric',
          minute: '2-digit',
          meridiem: false,
			events: [
              @foreach($reservations as $reservation)
               @if(!strcmp($reservation->statuses->name,"Realizado"))
               {
                  display: 'block',
                  borderColor:"@if($reservation->is_web==1) #ccc903 @else {{$reservation->employees->color}} @endif ",
				          backgroundColor:"{{$reservation->employees->color}}",
                  title: '(R){{$reservation->services->name }}, {{$reservation->pacients->name }} {{$reservation->pacients->lastname}}',
                  url: '{{route("admin.edit.reservation",[$reservation->id,0])}}',
                  start:"{{$reservation->date_at.'T'.date('H:i:s', strtotime($reservation->time_at))}}",
                  end: "{{$reservation->date_at.'T'.date('H:i:s', strtotime($reservation->time_at))}}",
                },
               @else
               {
                  display: 'block',
                  borderColor:"@if($reservation->is_web==1) #ccc903 @else {{$reservation->employees->color}} @endif ",
                  backgroundColor:"{{$reservation->employees->color}}",
                  title: '{{$reservation->services->name }},{{$reservation->pacients->name }} {{$reservation->pacients->lastname}}',
                  url: '{{route("admin.edit.reservation",[$reservation->id,0])}}',
                  start:"{{$reservation->date_at.'T'.date('H:i:s', strtotime($reservation->time_at))}}",
                  end: "{{$reservation->date_at.'T'.date('H:i:s', strtotime($reservation->time_at))}}",
                },
               @endif
             @endforeach
            ],
            dateClick: function(info) {
              url=" {{route('admin.edit.reservation',['reservation_id'=>0,'date'=>1])}}";
              url=url.replace('1',info.dateStr);
              location.href=url;
              // change the day's background color just for fun
              info.dayEl.style.backgroundColor = 'gren';
          }
          
           
         
          
          

        });
        calendar.setOption('locale', 'es');
        calendar.render();

        @if(Auth::user() && Auth::user()->customers->getConfig("feature/calendarCustomerByDay"))
  $(document).on("click",".fc-timeGridDay-button",function(){
    var url= "{{route('admin.gridDay.reservation',['day'=>0])}}"
    var moment=calendar.getDate();
    var date=moment.getFullYear().toString()+"-"+calendar.formatDate(moment,{month: 'numeric',}).toString()+"-"+moment.getUTCDate().toString()
    url=url.replace('0',date);
    $(".fc-timeGridDay-view ",calendarEl).html("")
    $.get(url,function(data){
      $(".fc-timeGridDay-view ",calendarEl).html(data)
    });
    $(".fc-view-harness-active").css("height","95vh")
 
  })

  $(document).on("click",".fc-next-button",function(){
    var moment=calendar.getDate();
    var date=moment.getFullYear().toString()+"-"+calendar.formatDate(moment,{month: 'numeric',}).toString()+"-"+moment.getUTCDate().toString()
    var url= "{{route('admin.gridDay.reservation',['day'=>0])}}"
    url=url.replace('0',date);
    $(".fc-timeGridDay-view ",calendarEl).html("")
    $.get(url,function(data){
      $(".fc-timeGridDay-view ",calendarEl).html(data)
    });
    $(".fc-view-harness-active").css("height","95vh")

  })
  $(document).on("click",".fc-prev-button",function(){
    var moment=calendar.getDate();
    var date=moment.getFullYear().toString()+"-"+calendar.formatDate(moment,{month: 'numeric',}).toString()+"-"+moment.getUTCDate().toString()
    var url= "{{route('admin.gridDay.reservation',['day'=>0])}}"
    url=url.replace('0',date);
    $(".fc-timeGridDay-view ",calendarEl).html("")
    $.get(url,function(data){
      $(".fc-timeGridDay-view ",calendarEl).html(data)
    });
    $(".fc-view-harness-active").css("height","95vh")

  })
 // colocamos la vista por defecto en el día de hoy
 $(".fc-timeGridDay-button").click();
  @endif
 
</script>
@endsection