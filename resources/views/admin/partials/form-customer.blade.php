
<div class="form-group">
    <label for="inputEmail1" class="col-lg-2 control-label">Nombre*</label>
    <div class="col-md-6">
      <input type="text" name="name" value="@isset($customer) {{$customer->name}} @endisset" class="form-control" id="name" placeholder="Nombre">
    </div>
  </div>

  <div class="form-group">
    <label for="inputEmail1" class="col-lg-2 control-label">Ciudad*</label>
    <div class="col-md-6">
      <input type="text" name="city" value="@isset($customer){{$customer->city}} @endisset" class="form-control" id="city" placeholder="city">
    </div>
  </div>
  
  <div class="form-group">
    <label for="inputEmail1" class="col-lg-2 control-label">Dirección*</label>
    <div class="col-md-6">
      <input type="text" name="description" value="@isset($customer){{$customer->description}} @endisset" class="form-control" id="description" placeholder="description">
    </div>
  </div>
  <div class="form-group">
    <label for="inputEmail1" class="col-lg-2 control-label">Telefono</label>
    <div class="col-md-6">
      <input type="text" name="amount" value="@isset($customer){{$customer->amount}} @endisset" class="form-control" id="amount" placeholder="amount">
    </div>
  </div>
  <div class="form-group">
    <label for="inputEmail1" class="col-lg-2 control-label">Keyname*</label>
    <div class="col-md-6">
      <input type="text" name="keyname" value="@isset($customer){{$customer->keyname}} @endisset" class="form-control" id="keyname" placeholder="keyname">
    </div>
  </div>
  <div class="form-group">
    <label for="inputEmail1" class="col-lg-2 control-label">Nueva foto:*</label>
    <div class="col-md-6">
      <input type="file" name="photos"  class="form-control" id="photos" placeholder="Stock de producto">
    </div>
  </div>
  <div class="form-group">
    <label for="inputEmail1" class="col-lg-2 control-label">Foto actual: *</label>
    <div class="col-md-6">
      <a class="item productPhoto"  href="javascript:;" style="color: grey" data-id={{$customer->id}} data-photo="{{$customer->logo}}" data-photo_id="{{$customer->logo}}" >
        <img  class="img-responsive"  src="{{asset($customer->logo)}}" alt="">
        FOTO
      </a>
      </div>
  </div>
      <input type="hidden" name="customer" value="@isset($customer){{$customer->id}} @endisset" class="form-control" id="keyname" placeholder="keyname">
    
    

  <div class="form-group">
    <label for="inputEmail1" class="col-lg-2 control-label" ></label>
    <div class="col-md-6 col-lg-6">
    <label  >
       Es administrador
      <input type="checkbox" name="is_admin" @isset($customer)@if($customer->is_admin)checked @endif @endisset> 
    </label>

    </div>
  </div>

  