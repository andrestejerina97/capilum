<div class="form-group">
    <label for="inputEmail1" class="col-lg-2 control-label">Nombre*</label>
    <div class="col-md-6">
        <input type="text" name="name" value="@isset($employee) {{ $employee->name }} @endisset"
            class="form-control" id="name" placeholder="Nombre">
    </div>
</div>
<div class="form-group">
    <label for="inputEmail1" class="col-lg-2 control-label">Apellido*</label>
    <div class="col-md-6">
        <input type="text" name="lastname"
            value="@isset($employee) {{ $employee->lastname }} @endisset" required
            class="form-control" id="lastname" placeholder="Apellido">
    </div>
</div>


<div class="form-group">
    <label for="inputEmail1" class="col-lg-2 control-label">Direccion*</label>
    <div class="col-md-6">
        <input type="text" name="address"
            value="@isset($employee) {{ $employee->address }} @endisset" class="form-control"
            required id="employeename" placeholder="Direccion">
    </div>
</div>
<div class="form-group">
    <label for="inputEmail1" class="col-lg-2 control-label">Email*</label>
    <div class="col-md-6">
        <input type="text" name="email" value="@isset($employee) {{ $employee->email }} @endisset"
            class="form-control" id="email" placeholder="Email">
    </div>
</div>

<div class="form-group">
    <label for="inputEmail1" class="col-lg-2 control-label">Telefono</label>
    <div class="col-md-6">
        <input type="text" name="phone" value="@isset($employee) {{ $employee->phone }} @endisset"
            class="form-control" id="inputEmail1" placeholder="Telefono">
    </div>
</div>
<div class="form-group">
    <label for="inputEmail1" class="col-lg-2 control-label">Color:</label>
    <div class="col-md-6">
        <input type="color" name="color" class="form-control"
            value="@isset($employee) {{ $employee->color }} @endisset" id="color"
            placeholder="color..">
    </div>
</div>

<div class="form-group">
    <label for="inputEmail1" class="col-lg-2 control-label">Hora inicio:</label>
    <div class="col-md-6">
        <input type="time" name="time_open" class="form-control" value="@isset($employee){{ $employee->time_open }}@endisset"
                id="color" placeholder="Hora inicio..">
        </div>
</div>

<div class="form-group">
    <label for="inputEmail1" class="col-lg-2 control-label">Hora fin:</label>
    <div class="col-md-6">
        <input type="time" name="time_close" class="form-control" value="@isset($employee){{ $employee->time_close }}@endisset"
                id="time_close" placeholder="Hora fin..">
        </div>
</div>

<div class="form-group">
    <script type="text/javascript" src="{{asset('js/jquery.timepicker.min.js')}}"></script>
    <link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/jquery-timepicker/1.2.17/jquery.timepicker.min.css"/>
    <script type="text/javascript" src="{{asset('js/jquery.businessHours.min.js')}}"></script>
    <link rel="stylesheet" type="text/css" href="{{asset('css/jquery.businessHours.css')}}"/>
    <link rel="stylesheet" type="text/css" href="{{asset('css/page.css')}}"/>

    <label for="inputEmail1" class="col-lg-2 control-label">Horarios de atención* </label>
    @php
     $openings= isset($employee)? json_decode($employee->times) :"" ;
    @endphp
    <div class="col-md-10">
        <div id="openingHours" ></div>
        <textarea name='times' id='openingHoursInput' class="hide"></textarea>

    </div>
    <script>
      var operationTime = @isset($employee) {!!$employee->times!!} @else `[
    {"isActive":false,"timeFrom":null,"timeTill":null},
    {"isActive":false,"timeFrom":null,"timeTill":null},
    {"isActive":false,"timeFrom":null,"timeTill":null},
    {"isActive":false,"timeFrom":null,"timeTill":null},
    {"isActive":false,"timeFrom":null,"timeTill":null},
    {"isActive":false,"timeFrom":null,"timeTill":null},
    {"isActive":false,"timeFrom":null,"timeTill":null}
    ];` @endisset

            window.businessHoursManager=$("#openingHours").businessHours({
            operationTime: operationTime,           // list of JSON objects

            weekdays: ['Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab', 'Dom'],
            dayTmpl: '<div class="dayContainer" style="width: 80px;">' +
                    '<div data-original-title="" class="colorBox"><input type="checkbox" class="invisible operationState"></div>' +
                    '<div class="weekday"></div>' +
                    '<div class="operationDayTimeContainer">' +
                    '<div class="operationTime input-group"><span class="input-group-addon"><i class="fa fa-sun-o"></i></span><input type="text" name="startTime" class="mini-time form-control operationTimeFrom" value=""></div>' +
                    '<div class="operationTime input-group"><span class="input-group-addon"><i class="fa fa-moon-o"></i></span><input type="text" name="endTime" class="mini-time form-control operationTimeTill" value=""></div>' +
                    '<div class="operationTime input-group hidden" style="display: table;"><span class="input-group-addon"><i class="fa fa-sun-o"></i></span><input type="text" name="startTime_1" class="mini-time form-control operationTimeFrom" value=""></div>' +
                    '<div class="operationTime input-group hidden" style="display: table;"><span class="input-group-addon"><i class="fa fa-moon-o"></i></span><input type="text" name="endTime_1" class="mini-time form-control operationTimeTill" value=""></div>' +
                    '</div><a class="btn btn-default btn-sm addTimeSlot"><span class="fa fa-plus-square"></span> Add</a></div>'
        });
    </script>

</div>
<div class="form-group">
    <label for="inputEmail1" class="col-lg-2 control-label">Foto :</label>
</div>
<div class="form-group">
    <div class="col-lg-2"></div>
    <div class="col-lg-10 col-md-6">
        <input type="file" name="photo" class="form-control" style="opacity: 100;
          position: relative;" id="photo" placeholder="Carrusel foto">
    </div>
</div>



<div class="form-group text-center show">
    @isset($employee->photo)
        <label for="inputEmail1" class="col-lg-2 control-label"> Foto actual</label>
        <div class="col-lg-8">
            <a class="item carouselPhoto" href="javascript:;" style="color: grey" data-id={{ $employee->id }}
                data-photo="{{ $employee->photo }}" data-photo_id="{{ $employee->id }}">
                <img class="img-responsive" src="{{ asset($employee->photo) }}" alt="">
            </a>
        </div>
    @endisset
</div>
