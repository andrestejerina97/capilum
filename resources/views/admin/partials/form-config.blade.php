<div class="form-group">
    <div class="form-group">
        <label for="inputEmail1" class="col-lg-2 control-label">Nombre*</label>
        <div class="col-md-6">
          <input type="text" name="" value="@isset($config) {{$config->name}} @endisset" class="form-control" id="email" placeholder="Nombre" disabled>
        </div>
      </div>
@isset($config)
<div class="form-group">
@switch($config->path)
    @case("email/birthdayMessage")
    <label for="inputEmail1" class="col-lg-2 control-label">{{$config->name}}*</label>
    <div class="col-md-6">
        <textarea name="value" id="value" class="form-control">@isset($config) {{$config->value}} @endisset</textarea>
    </div>
    @break
    @case("email/reminderMessage")
    <label for="inputEmail1" class="col-lg-2 control-label">{{$config->name}}*</label>
    <div class="col-md-6">
        <textarea name="value" id="value" class="form-control">@isset($config) {{$config->value}} @endisset</textarea>
    </div>
    @break
    @case("email/sendBirthdayMessage")
    <label for="inputEmail1" class="col-lg-2 control-label">{{$config->name}}*</label>
    <div class="col-md-6">
        <div class="radio">
            <label>
              <input type="radio" name="value" id="" value="0" @if($config->value==0)checked @endif>
                No
            </label>
          </div>
          <div class="radio">
            <label>
              <input type="radio" name="value" id="optionsRadios2" value="1" @if($config->value==1)checked @endif>
              Si
            </label>
          </div>
    </div>
    @break
    @case("email/sendReminderMessage")
    <label for="inputEmail1" class="col-lg-2 control-label">{{$config->name}}*</label>
    <div class="col-md-6">
        <div class="radio">
            <label>
              <input type="radio" name="value" id="" value="0" @if($config->value==0)checked @endif>
                No
            </label>
          </div>
          <div class="radio">
            <label>
              <input type="radio" name="value" id="optionsRadios2" value="1" @if($config->value==1)checked @endif>
              Si
            </label>
          </div>
    </div>
    @break
    @case("email/confirmReservationMessage")
    <label for="inputEmail1" class="col-lg-2 control-label">{{$config->name}}*</label>
    <div class="col-md-6">
        <textarea name="value" id="value" class="form-control">@isset($config) {{$config->value}} @endisset</textarea>
    </div>
    @break
    @case("email/sendConfirmReservation")
    <label for="inputEmail1" class="col-lg-2 control-label">{{$config->name}}*</label>
    <div class="col-md-6">
        <div class="radio">
            <label>
              <input type="radio" name="value" id="" value="0" @if($config->value==0)checked @endif>
                No
            </label>
          </div>
          <div class="radio">
            <label>
              <input type="radio" name="value" id="optionsRadios2" value="1" @if($config->value==1)checked @endif>
              Si
            </label>
          </div>
    </div>
    @break
    @case("feature/calendarCustomerByDay")
    <label for="inputEmail1" class="col-lg-2 control-label">{{$config->name}}*</label>
    <div class="col-md-6">
        <div class="radio">
            <label>
              <input type="radio" name="value" id="" value="0" @if($config->value==0)checked @endif>
                No
            </label>
          </div>
          <div class="radio">
            <label>
              <input type="radio" name="value" id="optionsRadios2" value="1" @if($config->value==1)checked @endif>
              Si
            </label>
          </div>
    </div>
    @break
    @case("feature/openingHours")
    <script type="text/javascript" src="{{asset('js/jquery.timepicker.min.js')}}"></script>
    <link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/jquery-timepicker/1.2.17/jquery.timepicker.min.css"/>
    <script type="text/javascript" src="{{asset('js/jquery.businessHours.min.js')}}"></script>
    <link rel="stylesheet" type="text/css" href="{{asset('css/jquery.businessHours.css')}}"/>
    <link rel="stylesheet" type="text/css" href="{{asset('css/page.css')}}"/>

    <label for="inputEmail1" class="col-lg-2 control-label">{{$config->name}}* </label>
    @php
     $openings= json_decode($config->value);
    @endphp
    <div class="col-md-10">
        <div id="openingHours" ></div>
        <textarea name='value' id='openingHoursInput' class="hide"></textarea>

    </div>
    <script>
      var operationTime = {!!$config->value!!}

            window.businessHoursManager=$("#openingHours").businessHours({
            operationTime: operationTime,           // list of JSON objects

            weekdays: ['Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab', 'Dom'],
            dayTmpl: '<div class="dayContainer" style="width: 80px;">' +
                    '<div data-original-title="" class="colorBox"><input type="checkbox" class="invisible operationState"></div>' +
                    '<div class="weekday"></div>' +
                    '<div class="operationDayTimeContainer">' +
                    '<div class="operationTime input-group"><span class="input-group-addon"><i class="fa fa-sun-o"></i></span><input type="text" name="startTime" class="mini-time form-control operationTimeFrom" value=""></div>' +
                    '<div class="operationTime input-group"><span class="input-group-addon"><i class="fa fa-moon-o"></i></span><input type="text" name="endTime" class="mini-time form-control operationTimeTill" value=""></div>' +
                    '<div class="operationTime input-group hidden" style="display: table;"><span class="input-group-addon"><i class="fa fa-sun-o"></i></span><input type="text" name="startTime_1" class="mini-time form-control operationTimeFrom" value=""></div>' +
                    '<div class="operationTime input-group hidden" style="display: table;"><span class="input-group-addon"><i class="fa fa-moon-o"></i></span><input type="text" name="endTime_1" class="mini-time form-control operationTimeTill" value=""></div>' +
                    '</div><a class="btn btn-default btn-sm addTimeSlot"><span class="fa fa-plus-square"></span> Add</a></div>'
        });
    </script>
    @break
    @default
    <label for="">Esta configuración no existe</label>
@endswitch
</div>
@endisset


 
