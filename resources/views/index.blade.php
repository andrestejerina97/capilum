@extends('layouts.admin')
@section('css')
 
<link href='{{asset('calendar5/main.css')}}' rel='stylesheet' />
<style>

</style>
@endsection
@section('content')
<div class="row">
	<div class="">
		<div class="card">
  		<div class="card-header" id="top">
      	<h4 class="title" id="encabezados">Mi agenda online</h4>
  		</div>
  		<div class="table-responsive" >
      <div id="calendar"></div>

      </div>
    
		</div>
	</div>
</div>
 
@endsection
@section('scripts')
<script src='{{asset("calendar5/main.js")}}'></script>
<script src='{{asset("calendar5/es.js")}}'></script>

<script>
	  var calendarEl = document.getElementById('calendar');
        var calendar = new FullCalendar.Calendar(calendarEl, {
          themeSystem: 'standard',
          timeZone: 'local',
           initialDate: "{{date('Y-m-d')}}", // will be parsed as local
		   initialView: 'dayGridMonth',     
		   editable: false,
           selectable: true,
           businessHours: true,
		   locale: 'es',
           dayMaxEvents: false, // allow "more" link when too many events
           hiddenDays:[0,6],
		   eventColor: '#378006',
          headerToolbar: { 
             left: 'prev',
			        center: 'dayGridMonth,dayGridWeek,timeGridDay' ,// buttons for switching between views
              right: 'next',
            },
              eventDisplay: 'block',
              hour: 'numeric',
          minute: '2-digit',
          meridiem: false,
			events: [
              @foreach($reservations as $reservation)
                {
					
				 color:"{{$reservation->employees->color}}",
                  title: '{{$reservation->pacients->name }} {{$reservation->pacients->lastname}}',
                  url: '{{route("admin.edit.reservation",["id"=>$reservation->id])}}',
                  start:"{{$reservation->date_at.'T'.date('H:i:s', strtotime($reservation->time_at))}}",
                  end: "{{$reservation->date_at.'T'.date('H:i:s', strtotime($reservation->time_at))}}",
                },
             @endforeach
            ],
            dateClick: function(info) {
				url=" {{route('admin.edit.reservation',['id'=>0,'date'=>1])}}";
				url=url.replace('1',info.dateStr);
			console.log(info.dateStr);
			location.href=url;
            // change the day's background color just for fun
            info.dayEl.style.backgroundColor = 'gren';
          }
          
           
         
          
          

        });
        calendar.setOption('locale', 'es');
  calendar.render();


</script>
@endsection