
@extends('layouts.app')
@section('content')
<br><br><br><br><br>

<div class="d-flex" id="login">
    <div class="row">
            <div class="col-md-4 col-md-offset-4">
            <?php if(isset($_COOKIE['password_updated'])):?>
                <div class="alert alert-success">
                <p><i class='glyphicon glyphicon-off'></i> Se ha cambiado la contraseña exitosamente !!</p>
                <p>Pruebe iniciar sesion con su nueva contraseña.</p>
    
                </div>
            <?php setcookie("password_updated","",time()-18600);
             endif; ?>
    
    <div class="card" style="font-family: elsie">
      <div class="card-header" style="background-color: #316ceb;">
          <h3 class="title" style="color:#ffffff; font-family: elsie">Acceder al Sistema           <img src="{{asset('app/logoS.png')}}" style="background:transparent;border-radius: 30px; max-height: 50px; min-height: 50px; max-width: 50px;" loading="lazy" alt="">
          </h3>

        </div>
      <div class="card-content table-responsive">
        <form method="POST" action="{{ route('login') }}">
            @csrf
                        <fieldset>
                              <div class="form-group">
                                <input id="email" placeholder="Usuario" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                            </div>
                            <input class="form-check-input" hidden type="checkbox" name="remember" id="remember" checked>

                            <div class="form-group">
                                <input id="password" placeholder="Password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">
                                @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                            </div>
                            <input class="btn btn-primary btn-block" type="submit" style="background-color: #316ceb" value="Iniciar Sesion">
                        </fieldset>
                          </form>
                          <div class="row text-center">
                            <h5>¿No tenés cuenta? Solicitala desde <a href="https://wa.me/543516687683?text=Hola,%20tengo%20una%20consulta%20sobre%20Turnero">AQUÍ</a></h5>
                          </div>
                          </div>
                          </div>
                          
            </div>
        </div>
    </div>
    
@endsection

