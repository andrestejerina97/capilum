<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnDelete extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('services', function (Blueprint $table) {
            $table->softDeletes();
        });
        Schema::table('employees', function (Blueprint $table) {
            $table->softDeletes();
        
        });  
        Schema::table('pacients', function (Blueprint $table) {
        
            $table->softDeletes();
        });
        Schema::table('users', function (Blueprint $table) {
            $table->softDeletes();
            });
        Schema::table('reservations', function (Blueprint $table) {
                $table->softDeletes();
                });
        Schema::table('customers', function (Blueprint $table) {
                    $table->softDeletes();
                    });
     
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('reservations', function (Blueprint $table) {
            $table->dropSoftDeletes();  
            });
        Schema::table('services', function (Blueprint $table) {
            $table->dropSoftDeletes();  
              });
        Schema::table('employees', function (Blueprint $table) {
            $table->dropSoftDeletes();        });  
        Schema::table('pacients', function (Blueprint $table) {
            $table->dropSoftDeletes();        });
        Schema::table('users', function (Blueprint $table) {
            $table->dropSoftDeletes();
                });
        Schema::table('customers', function (Blueprint $table) {
            $table->dropSoftDeletes();
            });
    }
}
