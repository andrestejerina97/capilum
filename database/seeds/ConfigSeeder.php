<?php

use App\Config;
use App\customer;
use Illuminate\Database\Seeder;

class ConfigSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $customers = customer::all();
        foreach ($customers as $customer) {
            if (!Config::where("customer_id", $customer->id)->where("path", "email/birthdayMessage")->first()) {
                Config::insert([
                    [
                        "name" => "Mensaje recordatorio de cumpleaños",
                        "path" => "email/birthdayMessage",
                        "value" => "Estimado @nombre_cliente te deseamos un feliz cumpleaños de parte de CALENDY SA",
                        "customer_id" => $customer->id
                    ]
                ]);
            }
            if (!Config::where("customer_id", $customer->id)->where("path", "email/reminderMessage")->first()) {
                Config::insert([
                    [
                        "name" => "Mensaje recordatorio de turnos",
                        "path" => "email/reminderMessage",
                        "value" => "Estimado @nombre_cliente te recordamos que tenés un turno reservado para el día @fecha_turno ,  te esperamos en nuestro local",
                        "customer_id" => $customer->id
                    ]
                ]);
            }
            if (!Config::where("customer_id", $customer->id)->where("path", "email/sendBirthdayMessage")->first()) {
                Config::insert([
                    [
                        "name" => "Habilitar envío automático de mensaje de cumpleaños",
                        "path" => "email/sendBirthdayMessage",
                        "value" => "0",
                        "customer_id" => $customer->id
                    ]
                ]);
            }
            if (!Config::where("customer_id", $customer->id)->where("path", "email/sendReminderMessage")->first()) {
                Config::insert([
                    [
                        "name" => "Habilitar envío automático de recordatorio de turnos",
                        "path" => "email/sendReminderMessage",
                        "value" => "0",
                        "customer_id" => $customer->id
                    ]
                ]);
            }

            // Nuevo Email de confirmación de turno
            if (!Config::where("customer_id", $customer->id)->where("path", "email/sendConfirmReservation")->first()) {
                Config::insert([
                    [
                        "name" => "Habilitar envío automático de confirmación de turno",
                        "path" => "email/sendConfirmReservation",
                        "value" => "0",
                        "customer_id" => $customer->id
                    ]
                ]);
            }
            if (!Config::where("customer_id", $customer->id)->where("path", "email/confirmReservationMessage")->first()) {
                Config::insert([
                    [
                        "name" => "Mensaje de confirmación de turno",
                        "path" => "email/confirmReservationMessage",
                        "value" => "Estimado @nombre_cliente tu turno se confirmó con éxito el día @fecha_turno ,  te esperamos en nuestro local",
                        "customer_id" => $customer->id
                    ]
                ]);
            }
            if (!Config::where("customer_id", $customer->id)->where("path", "feature/calendarCustomerByDay")->first()) {
                Config::insert([
                    [
                        "name" => "Vista calendario formato excel",
                        "path" => "feature/calendarCustomerByDay",
                        "value" => "0",
                        "customer_id" => $customer->id
                    ]
                ]);
            }
            if (!Config::where("customer_id", $customer->id)->where("path", "feature/openingHours")->first()) {
                Config::insert([
                    [
                        "name" => "Horarios de atención",
                        "path" => "feature/openingHours",
                        "customer_id" => $customer->id
                    ]
                ]);
            }
        }
    }
}
