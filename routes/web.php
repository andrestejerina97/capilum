<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteUserProvider within a group which
| contains the "web" middleware group. Now create something great!
|
define('STDIN',fopen("php://stdin","r"));
Route::get('install', function() {
    Artisan::call('migrate',['--force'=>true]);
});
*/


Route::namespace('admin')->middleware('auth')->group(function(){
    Route::get('/', 'HomeController@index')->name('home2');

    //Cliente root
    Route::get('/root/admin', 'RootController@root')->name('root');
    Route::post('/root/admin-guardar', 'RootController@save')->name('root.store');
    Route::post('/root/admin-editar', 'RootController@update')->name('root.update');
    Route::get('/root/editar/{id}', 'RootController@edit')->name('root.edit');

    //clientes
    Route::get('/clientes','PacientController@index')->name('admin.index.pacient');
    Route::get('/mi-cliente/{pacient_id?}','PacientController@edit')->name('admin.edit.pacient');
    Route::post('/guardar-cliente/{id?}','PacientController@store')->name('admin.store.pacient');
    Route::post('/actualizar-cliente/{id?}','PacientController@update')->name('admin.update.pacient');
    Route::get('/borrar-cliente/{id?}','PacientController@delete')->name('admin.delete.pacient');
    Route::get('/cliente-filtro','PacientController@search')->name('admin.search.pacient');
    Route::get('/ver-mi-cliente/{pacient_id?}','PacientController@show')->name('admin.show.pacient');

    //empleados
    Route::get('/empleados','EmployeeController@index')->name('admin.index.employee');
    Route::get('/mi-empleado/{employee_id?}','EmployeeController@edit')->name('admin.edit.employee');
    Route::post('/guardar-empleado/{id?}','EmployeeController@store')->name('admin.store.employee');
   // Route::post('/actualizar-empleado/{id?}','EmployeeController@update')->name('admin.update.employee');
    Route::get('/eliminar-empleado/{id?}','EmployeeController@delete')->name('admin.delete.employee');
    Route::get('/empleado-filtro','EmployeeController@search')->name('admin.search.employee');
    
    //reservaciones
    Route::get('/turnos','ReservationController@index')->name('admin.index.reservation');
    Route::get('/mi-turno-actual/{reservation_id?}/{date?}','ReservationController@edit')->name('admin.edit.reservation');
    Route::post('/guardar-turno-/{id?}','ReservationController@store')->name('admin.store.reservation');
    Route::post('/actualizar-turno/{id?}','ReservationController@update')->name('admin.update.reservation');
    Route::get('/eliminar-turno/{id?}','ReservationController@delete')->name('admin.delete.reservation');
    Route::post('/turno-filtro','ReservationController@search')->name('admin.search.reservation');
    Route::get('/filtro-turno','ReservationController@search')->name('admin.search.reservation');
    Route::get('/ver-reservacion/{modal?}/{pacient_id}','ReservationController@show')->name('admin.show.reservation');
    Route::get('reservation/grid-day/{day}','ReservationController@gridDay')->name('admin.gridDay.reservation');

    //Servicios
    Route::get('/servicio','ServiceController@index')->name('admin.index.service');
    Route::get('/mi-servicio-/{service_id?}','ServiceController@edit')->name('admin.edit.service');
    Route::post('/guardar-servicio/{id?}','ServiceController@store')->name('admin.store.service');
    Route::get('/eliminar-servicio/{id?}','ServiceController@delete')->name('admin.delete.service');
    Route::get('/servicio-filtro','ServiceController@search')->name('admin.search.service');
    
    //usuarios
    Route::get('/usuarios','UserController@index')->name('admin.index.user');
    Route::get('/mi-usuario/{user_id?}','UserController@edit')->name('admin.edit.user');
    Route::post('/guardar-usuario-/{id?}','UserController@store')->name('admin.store.user');
    Route::get('/eliminar-usuario/{id?}','UserController@delete')->name('admin.delete.user');
    Route::get('/servicio-usuario','UserController@search')->name('admin.search.user');
    Route::get('/home', 'HomeController@index')->name('home');
    Route::get('/crear', 'HomeController@crear')->name('craea');

    //Configuraciones
    Route::get('/configuraciones','ConfigController@index')->name('admin.index.config');
    Route::get('/mi-config/{config_id?}','ConfigController@edit')->name('admin.edit.config');
    Route::post('/guardar-config/{id?}','ConfigController@store')->name('admin.store.config');
    Route::get('/eliminar-config/{id?}','ConfigController@delete')->name('admin.delete.config');
    Route::get('/servicio-config','ConfigController@search')->name('admin.search.config');

});

Auth::routes();

